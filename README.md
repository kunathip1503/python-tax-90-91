# โปรแกรมคำนวณภาษีเงินได้บุคคลธรรมดา

โปรแกรมนี้ถูกสร้างขึ้นเพื่อช่วยในการคำนวณภาษีเงินได้สำหรับบุคคลธรรมดาในประเทศไทย โดยการให้ข้อมูลเกี่ยวกับรายได้ อายุ สถานภาพการสมรส และหลายรายการลดหย่อนอื่นๆ เพื่อคำนวณภาษีที่ต้องจ่ายตามกฎหมายภาษีบัณฑิต

## วิธีการใช้งาน

1. กรอกข้อมูลเกี่ยวกับรายได้และข้อมูลส่วนบุคคลที่จำเป็นในฟิลด์ที่มีให้
2. กดปุ่ม "คำนวณภาษี"
3. ผลลัพธ์จะแสดงในส่วนด้านล่างของหน้าต่างโปรแกรม

## คำแนะนำ

- รายได้ต้องเป็นค่าจำนวนเงินที่เป็นจริงต่อปี
- ค่าลดหย่อนต้องระบุให้ถูกต้องเพื่อให้ผลลัพธ์ที่ถูกต้อง

## การติดตั้งและการใช้งาน

1. ดาวน์โหลดไฟล์ [tax_calculator.py](tax_calculator.py)
2. เปิด Command Line Interface (CLI) หรือ Terminal
3. เข้าไปยังไดเรกทอรีที่บันทึกไฟล์ `tax_calculator.py`
4. ใช้คำสั่ง `python tax_calculator.py` เพื่อเริ่มต้นใช้งานโปรแกรม

## ความต้องการระบบ

- Python 3.x
- Tkinter (มาพร้อมกับ Python 3.x)

## ข้อจำกัด

- โปรแกรมนี้สำหรับการคำนวณภาษีเงินได้บุคคลธรรมดาในประเทศไทยเท่านั้น
- ผลลัพธ์ที่ได้อาจมีความคลาดเคลื่อน แนะนำให้ตรวจสอบกับที่ปรึกษาภาษีหรือเจ้าหน้าที่ทางภาษี

## แหล่งอ้างอิง

- [กฎหมายภาษีบัณฑิต](https://www.rd.go.th/rd/publish/fileadmin/user_upload/pdf/personal_income_tax_b_e_2564.pdf)
- [สำนักงานสรรพากร](https://www.rd.go.th/)

=======