import math
import tkinter as tk
from tkinter import messagebox


def calculate_personal_income_tax(taxable_income):
    tax_rates = [
        (150000, 0),
        (300000, 0.05),
        (500000, 0.10),
        (750000, 0.15),
        (1000000, 0.20),
        (2000000, 0.25),
        (5000000, 0.30),
        (math.inf, 0.35),
    ]

    tax = 0
    prev_bracket = 0
    for bracket, rate in tax_rates:
        if taxable_income <= bracket:
            tax += (taxable_income - prev_bracket) * rate
            break
        else:
            tax += (bracket - prev_bracket) * rate
            prev_bracket = bracket

    return tax


def calculate_personal_allowance(
    age,
    spouse_allowance,
    education_allowance,
    child_allowance,
    parent_allowance,
    health_insurance_allowance,
    life_insurance_allowance,
    ltf_rmf_allowance,
    social_security_allowance,
    home_loan_interest_allowance,
):
    personal_allowance = 60000

    if age >= 65:
        personal_allowance += 190000
    elif age >= 40:
        personal_allowance += 100000

    personal_allowance += (
        spouse_allowance
        + education_allowance
        + child_allowance
        + parent_allowance
        + health_insurance_allowance
        + life_insurance_allowance
        + ltf_rmf_allowance
        + social_security_allowance
        + home_loan_interest_allowance
    )

    return personal_allowance


def calculate_tax():
    try:
        income = float(income_entry.get())
        age = int(age_entry.get())
        spouse_allowance = 60000 if married_var.get() else 0
        education_allowance = float(education_entry.get())
        num_children = int(children_entry.get())
        child_allowance = min(num_children, 3) * 30000
        num_parents = int(parents_entry.get())
        parent_allowance = min(num_parents, 2) * 30000
        health_insurance_allowance = float(health_insurance_entry.get())
        life_insurance_allowance = float(life_insurance_entry.get())
        ltf_rmf_allowance = float(ltf_rmf_entry.get())
        social_security_allowance = float(social_security_entry.get())
        home_loan_interest_allowance = float(home_loan_interest_entry.get())

        personal_allowance = calculate_personal_allowance(
            age,
            spouse_allowance,
            education_allowance,
            child_allowance,
            parent_allowance,
            health_insurance_allowance,
            life_insurance_allowance,
            ltf_rmf_allowance,
            social_security_allowance,
            home_loan_interest_allowance,
        )
        taxable_income = max(0, income - personal_allowance)
        tax = calculate_personal_income_tax(taxable_income)

        result_text.set(
            f"เงินได้สุทธิ: {taxable_income:.2f} บาท\nภาษีเงินได้บุคคลธรรมดา: {tax:.2f} บาท"
        )
    except ValueError:
        messagebox.showerror("Error", "กรุณาใส่ข้อมูลให้ถูกต้อง")


# สร้างหน้าต่างหลัก
window = tk.Tk()
window.title("โปรแกรมคำนวณภาษีเงินได้บุคคลธรรมดา")

# สร้างฟิลด์อินพุต
income_label = tk.Label(window, text="รายได้ต่อปี:")
income_label.pack()
income_entry = tk.Entry(window)
income_entry.pack()

age_label = tk.Label(window, text="อายุ:")
age_label.pack()
age_entry = tk.Entry(window)
age_entry.pack()

married_var = tk.BooleanVar()
married_checkbutton = tk.Checkbutton(window, text="สมรสแล้ว", variable=married_var)
married_checkbutton.pack()

education_label = tk.Label(window, text="ค่าลดหย่อนการศึกษา:")
education_label.pack()
education_entry = tk.Entry(window)
education_entry.pack()

children_label = tk.Label(window, text="จำนวนบุตร:")
children_label.pack()
children_entry = tk.Entry(window)
children_entry.pack()

parents_label = tk.Label(window, text="จำนวนบิดามารดาที่อุปการะ:")
parents_label.pack()
parents_entry = tk.Entry(window)
parents_entry.pack()

health_insurance_label = tk.Label(window, text="เบี้ยประกันสุขภาพ:")
health_insurance_label.pack()
health_insurance_entry = tk.Entry(window)
health_insurance_entry.pack()

life_insurance_label = tk.Label(window, text="เบี้ยประกันชีวิต:")
life_insurance_label.pack()
life_insurance_entry = tk.Entry(window)
life_insurance_entry.pack()

ltf_rmf_label = tk.Label(window, text="ค่าลดหย่อน LTF/RMF:")
ltf_rmf_label.pack()
ltf_rmf_entry = tk.Entry(window)
ltf_rmf_entry.pack()

social_security_label = tk.Label(window, text="ค่าลดหย่อนประกันสังคม:")
social_security_label.pack()
social_security_entry = tk.Entry(window)
social_security_entry.pack()

home_loan_interest_label = tk.Label(window, text="ดอกเบี้ยเงินกู้เพื่อที่อยู่อาศัย:")
home_loan_interest_label.pack()
home_loan_interest_entry = tk.Entry(window)
home_loan_interest_entry.pack()

# สร้างปุ่มคำนวณ
calculate_button = tk.Button(window, text="คำนวณภาษี", command=calculate_tax)
calculate_button.pack()

# สร้างข้อความแสดงผลลัพธ์
result_text = tk.StringVar()
result_label = tk.Label(window, textvariable=result_text)
result_label.pack()

window.minsize(400, 500)  # กำหนดขนาดต่ำสุดเป็น 400x500 พิกเซล
window.maxsize(800, 800)  # กำหนดขนาดสูงสุดเป็น 800x800 พิกเซล

# เริ่มลูปหลักของแอปพลิเคชัน
window.mainloop()
